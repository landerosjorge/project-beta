import { useEffect, useState} from 'react';
import './MainPage.css';

function SalespeopleList () {
    const [salespersons, setSalespeople] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespersons)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='white-text'>
            <h1>Sales People</h1>
            <table className="table white-text">
                <thead>
                    <tr>
                        <th className='fs-3'>First Name</th>
                        <th className='fs-3'>Last Name</th>
                        <th className='fs-3'>Employee Id</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersons.map(salesperson => {
                        return (
                            <tr className='fw-normal' key={salesperson.id}>
                                <td className='fs-3'>{ salesperson.first_name }</td>
                                <td className='fs-3'>{ salesperson.last_name }</td>
                                <td className='fs-3'>{ salesperson.employee_id }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )

}

export default SalespeopleList;
