import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutoForm from './AutomobileForm';
import AutosList from './AutomobilesList';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelForm from './ModelForm';
import ModelsList from './ModelsList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import SalesPersonForm from './SalesPersonForm';
import SalespeopleList from './SalesPersonList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SaleForm from './SaleForm';
import SalesList from './SaleList';
import SalespersonHistory from './SalespersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="inventory/automobiles" element={<AutosList />} />
          <Route path="inventory/automobiles/new" element={<AutoForm />} />
          <Route path="inventory/manufacturers" element={<ManufacturerList />} />
          <Route path="inventory/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="inventory/models" element={<ModelsList />} />
          <Route path="inventory/models/new" element={<ModelForm />} />
          <Route path="service/technician/new" element={<TechnicianForm />} />
          <Route path="service/technician" element={<TechnicianList />} />
          <Route path="service/appointment/new" element={<AppointmentForm />} />
          <Route path="service/appointment" element={<AppointmentList />} />
          <Route path="service/history" element={<ServiceHistory />} />
          <Route path="sales/salesperson/new" element={<SalesPersonForm />} />
          <Route path="sales/salesperson" element={<SalespeopleList />} />
          <Route path="sales/customer/new" element={<CustomerForm />} />
          <Route path="sales/customer" element={<CustomerList />} />
          <Route path="sales/sale/new" element={<SaleForm />} />
          <Route path="sales/sale" element={<SalesList />} />
          <Route path="sales/sale/history" element={<SalespersonHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
