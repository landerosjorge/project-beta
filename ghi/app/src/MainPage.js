import React from 'react';
import './MainPage.css';

function MainPage() {
  return (
    <div className="page-content">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold white-text">EliteMotors</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4 white-text">
            The premiere solution for automobile dealership management!
          </p>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
