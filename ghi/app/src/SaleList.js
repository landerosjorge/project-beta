import { useEffect, useState} from 'react';
import './MainPage.css';

function SalesList () {
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='white-text'>
            <h1>Sales</h1>
            <table className="table white-text">
                <thead>
                    <tr>
                        <th className='fs-3'>Customer</th>
                        <th className='fs-3'>VIN</th>
                        <th className='fs-3'>Price</th>
                        <th className='fs-3'>Salesperson Name</th>
                        <th className='fs-3'>Salesperson ID</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr className='fw-normal' key={sale.id}>
                                <td className='fs-3'>{ sale.customer.first_name }</td>
                                <td className='fs-3'>{ sale.automobile.vin }</td>
                                <td className='fs-3'>{ sale.price }</td>
                                <td className='fs-3'>{ sale.salesperson.first_name }</td>
                                <td className='fs-3'>{ sale.salesperson.employee_id }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )

}

export default SalesList;
