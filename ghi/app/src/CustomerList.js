import { useEffect, useState} from 'react';
import './MainPage.css';

function CustomerList () {
    const [customers, setCustomers] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='white-text'>
            <h1>Customers</h1>
            <table className="table white-text">
                <thead>
                    <tr>
                        <th className='fs-3'>First Name</th>
                        <th className='fs-3'>Last Name</th>
                        <th className='fs-3'>Phone Number</th>
                        <th className='fs-3'>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return (
                            <tr className='fw-normal' key={customer.id}>
                                <td className='fs-3'>{ customer.first_name }</td>
                                <td className='fs-3'>{ customer.last_name }</td>
                                <td className='fs-3'>{ customer.phone_number }</td>
                                <td className='fs-3'>{ customer.address }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )

}

export default CustomerList;
